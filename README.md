# LibreWolf Issue Tracker & Update Tools 🚀

Welcome to the **LibreWolf for Windows** repository! This repository is now dedicated to the **Issue Tracker**. Submit your issues and get the latest updates on LibreWolf for Windows. 

To download the latest release, visit the [official LibreWolf release page](https://gitlab.com/librewolf-community/browser/bsys6/-/releases). For installation options, check out the [FAQ](https://librewolf.net/docs/faq/).

## Update Tools 🛠️

Stay secure and up-to-date with these LibreWolf update tools:

1. **LibreWolf WinUpdater**: Kudos to @ltguillaume for this [automatic updater](https://codeberg.org/ltguillaume/librewolf-winupdater) that can be set up to update LibreWolf automatically or run manually.

2. **LibreWolf Update Checker Extension**: Created by Defkev, this [extension](https://addons.mozilla.org/en-US/firefox/addon/librewolf-updater/) will notify you when an update is available and guide you to the download link.

> Please note: The *updater* can _install_ updates automatically, while the *extension* can only _check_ for updates.

## LibreWolf for Windows 🖥️

- LibreWolf supports multiple UI languages, available in settings.
- The latest **-portable.zip** release is self-contained and can be run on removable storage.

## Issue Tracker 🎫

- For issues with Settings or Advanced Settings (`about:config`), submit them to the [settings repository](https://gitlab.com/librewolf-community/settings/-/issues).
- For other issues, such as crashes/theme/graphics/speed problems, submit them to [issues for windows repository](https://gitlab.com/librewolf-community/browser/windows/-/issues).

## Building from Source 🏗️

- Our `bsys` build system supports cross-compiling from Linux to Windows. Building the Windows version from within Windows is not tested yet.
- The build system can be found here: [bsys6](https://gitlab.com/librewolf-community/browser/bsys6).
